# expenses-app #

This is a sample project using [AngularJS](https://angularjs.org/) and [Django REST Framework](http://www.django-rest-framework.org/) by [Michael Cetrulo](http://www.toptal.com/resume/michael-cetrulo#obtain-veteran-architects), made for the interview at [Toptal](http://www.toptal.com/#obtain-veteran-architects) and published for educational purposes (all rights reserved).

## Setup ##

To run this project:

* Install deps with `pip install -r requirements.txt`
* Create and fill `src/django/expenses/local_settings.py` (look at the example).
* Create the SQLite database and run migrations with `manage.py migrate`
* Start the web server with `manage.py runserver`

The web app will be running at: http://127.0.0.1:8000/ the API interface at http://127.0.0.1:8000/api/v1/ and the Django admin at: http://127.0.0.1:8000/admin/