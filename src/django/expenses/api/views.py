from datetime import timedelta
from django.contrib.auth import authenticate, login
from django.core.exceptions import PermissionDenied
from django.shortcuts import redirect, render
from rest_framework.decorators import api_view, list_route, permission_classes
from rest_framework.exceptions import ValidationError as DRFValidationError
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from expenses.api import models, serializers, permissions, forms


class UserViewSet(ModelViewSet):
    """ ViewSet for the User model """
    queryset = models.User.objects.all()
    serializer_class = serializers.UserSerializer
    permission_classes = (permissions.UserPerms,)


class ExpenseViewSet(ModelViewSet):
    """ ViewSet for the Expense model (nested on User) """
    serializer_class = serializers.ExpenseSerializer
    permission_classes = (permissions.ExpensePerms,)

    def get_queryset(self):
        owner = get_object_or_404(models.User.objects, pk=self.kwargs['user_pk'])

        self.filter_params = forms.ExpenseFilterForm(dict(
            (key.replace('-', '_'), val)
            for key, val in self.request.QUERY_PARAMS.items()
        ))

        if self.filter_params.is_valid():
            cleaned_data = self.filter_params.cleaned_data

            queryset = owner.expense_set

            if cleaned_data['from_date']:
                queryset = queryset.filter(datetime__gte=cleaned_data['from_date'])

            if cleaned_data['to_date']:
                queryset = queryset.filter(datetime__lt=cleaned_data['to_date'] + timedelta(days=1))

            if cleaned_data['description']:
                queryset = queryset.filter(description__icontains=cleaned_data['description'])

            if cleaned_data['comment']:
                queryset = queryset.filter(comment__icontains=cleaned_data['comment'])

            return queryset

        raise DRFValidationError(detail=self.filter_params.errors)

    @list_route()
    def print_report(self, request, **kwargs):
        context = {
            'expenses': self.get_queryset(),
        }
        # .get_queryset() populates self.filter_params
        context['filters'] = self.filter_params.cleaned_data

        context['total_expense'] = sum(
            expense.amount for expense in context['expenses']
        )

        from_date = context['filters']['from_date'] or min(
            expense.datetime for expense in context['expenses']
        )
        to_date = context['filters']['to_date'] or max(
            expense.datetime for expense in context['expenses']
        )
        delta = (to_date - from_date).days + 1

        context['average_expense'] = context['total_expense'] / delta

        return render(request, 'print_report.html', context)


@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def json_login(request, *args, **kwargs):
    credentials = {
        'username': request.data.get('username', ''),
        'password': request.data.get('password', ''),
    }

    user = authenticate(**credentials)
    if user is not None:
        login(request, user)

        serialized_user = serializers.UserSerializer(user, context={'request': request})
        return Response(serialized_user.data)

    raise PermissionDenied()


@api_view(['GET'])
@permission_classes((permissions.IsAuthenticated,))
def me(request, **kwargs):
    """ plain view to return own profile when authenticated """
    user = request.user

    kwargs.update({'pk': user.id})
    user_url = reverse('user-detail', kwargs=kwargs, request=request)

    return redirect(user_url)
