from django.conf.urls import url, include
from rest_framework import routers
from rest_framework.authtoken import views as authtoken_views

from expenses.api import views

router = routers.DefaultRouter(trailing_slash=False)
router.register(r'users', views.UserViewSet)
router.register(r'users/(?P<user_pk>[^/.]+)/expenses', views.ExpenseViewSet, base_name='expense')

urlpatterns = router.urls + [
    url(r'^api-token-auth/', authtoken_views.obtain_auth_token, name='obtain-token'),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^api-auth/login\.(?P<format>[a-z0-9]+)', views.json_login, name='json-login'),
    url(r'^me\.(?P<format>[a-z0-9]+)', views.me, name='me'),
]
