from django.contrib import admin
from expenses.api.models import Expense

admin.site.register(Expense)
