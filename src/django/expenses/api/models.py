from django.contrib.auth.models import User
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

from rest_framework.authtoken.models import Token


@receiver(post_save, sender='auth.User')
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        # attach auth token to newly-created User instance
        Token.objects.create(user=instance)


class Expense(models.Model):
    owner       = models.ForeignKey(User)
    datetime    = models.DateTimeField()
    description = models.CharField(max_length=120)
    amount      = models.DecimalField(max_digits=8, decimal_places=2)
    comment     = models.TextField(blank=True)

    def __unicode__(self):
        return u'Expense(%s, $%s at %s)' % (self.owner, self.amount, self.datetime)
