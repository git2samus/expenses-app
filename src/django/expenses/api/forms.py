from django import forms

datepicker_fmt = ['%Y-%m-%dT%H:%M:%S.000Z']


class ExpenseFilterForm(forms.Form):
    # dates should be required but conflicts with instance retrieval on API
    from_date   = forms.DateTimeField(required=False, input_formats=datepicker_fmt)
    to_date     = forms.DateTimeField(required=False, input_formats=datepicker_fmt)

    description = forms.CharField(required=False)
    comment    = forms.CharField(required=False)

    def clean(self):
        cleaned_data = super(ExpenseFilterForm, self).clean()
        from_date = cleaned_data.get('from_date')
        to_date = cleaned_data.get('to_date')

        if from_date and to_date and to_date < from_date:
            self.add_error('to_date', u"'to_date' cannot occur before 'from_date'")
