from rest_framework.serializers import ModelSerializer, HyperlinkedRelatedField

from expenses.api import models


class ExpenseHyperlinkedRelatedField(HyperlinkedRelatedField):
    def get_url(self, obj, view_name, request, format):
        if obj.pk is None:
            return None

        kwargs = {
            'user_pk': obj.owner_id, 'pk': obj.pk,
        }
        return self.reverse(view_name, kwargs=kwargs, request=request, format=format)


class UserSerializer(ModelSerializer):
    expenses = ExpenseHyperlinkedRelatedField(
        many=True,
        read_only=True,
        view_name='expense-detail',
        source='expense_set',
    )

    class Meta:
        model = models.User
        fields = ('id', 'username', 'password', 'first_name', 'last_name', 'expenses',)
        write_only_fields = ('password',)

    def create(self, validated_data):
        user = super(UserSerializer, self).create(validated_data)
        # hash password
        user.set_password(validated_data['password'])
        user.save()
        return user

    def update(self, instance, validated_data):
        user = super(UserSerializer, self).update(instance, validated_data)
        if 'password' in validated_data:
            # hash password
            user.set_password(validated_data['password'])
        user.save()
        return user


class ExpenseSerializer(ModelSerializer):
    class Meta:
        model = models.Expense
        exclude = ('owner',)

    def create(self, validated_data):
        # attach current user as Expense owner
        request = self.context['request']
        validated_data['owner'] = request.user

        return super(ExpenseSerializer, self).create(validated_data)
