from django.core.urlresolvers import reverse
from rest_framework import status

from expenses.api.test_modules.utils import FixtureTestCase


class ApiRootTestCase(FixtureTestCase):
    view_name = 'api-root'

    def test_guest(self):
        url = reverse(self.view_name)

        self.logout()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # POST
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # PUT
        response = self.client.put(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # PATCH
        response = self.client.patch(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user(self):
        url = reverse(self.view_name)

        self.login_user()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'users': 'http://testserver/api/v1/users'})

        # POST
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # PUT
        response = self.client.put(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # PATCH
        response = self.client.patch(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class AuthTokenTestCase(FixtureTestCase):
    def test_obtain_token(self):
        url = reverse('obtain-token')

        from django.contrib.auth.models import User
        from rest_framework.authtoken.models import Token

        user = User.objects.get(pk=self.user_data['id'])
        token = Token.objects.get(user=user)

        self.logout()

        credentials = {
            'username': 'user1',
            'password': 'user',
        }

        response = self.client.post(url, credentials, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {'token': token.key})

        # authenticate w/token
        self.client.credentials(HTTP_AUTHORIZATION='Token %s' % token.key)

        url = reverse('expense-list', kwargs={'user_pk': self.user_data['id']})

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        url = reverse('expense-list', kwargs={'user_pk': self.admin_data['id']})

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class MeTestCase(FixtureTestCase):
    def test_redirect(self):
        me_url = reverse('me', kwargs={'format': 'json'})

        self.login_user()
        user_url = reverse('user-detail', kwargs={'pk': self.user_data['id'], 'format': 'json'})

        response = self.client.get(me_url, format='json')
        self.assertRedirects(response, user_url)

        self.login_admin()
        admin_url = reverse('user-detail', kwargs={'pk': self.admin_data['id'], 'format': 'json'})

        response = self.client.get(me_url, format='json')
        self.assertRedirects(response, admin_url)

        self.logout()
        response = self.client.get(me_url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
