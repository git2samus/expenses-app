from django.core.urlresolvers import reverse
from rest_framework import status

from expenses.api.test_modules.utils import FixtureTestCase

class UsersTestCase(FixtureTestCase):
    """ a more extensive test for user administration via API """
    view_name = 'user-list'

    def test_guest(self):
        url = reverse(self.view_name)

        self.logout()

        # test required parameters
        new_user = {}
        response = self.client.post(url, new_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        new_user = {'username': 'new_user'}
        response = self.client.post(url, new_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        new_user = {'password': 'hunter2'}
        response = self.client.post(url, new_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # test minimal creation
        new_user = {
            'username':     'new_user',
            'password':     'hunter2',
        }

        response = self.client.post(url, new_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response_data = response.data
        del(response_data['id'])

        new_user_data = {
            'username':     'new_user',
            'first_name':   '',
            'last_name':    '',
            'expenses':     [],
        }

        self.assertEqual(response_data, new_user_data)

        # test duplicate
        new_user = {
            'username':     'new_user',
            'password':     'hunter2',
        }

        response = self.client.post(url, new_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # test full creation
        new_user = {
            'username':     'new_user_w_name',
            'password':     'hunter2',
            'first_name':   'first',
            'last_name':    'last',
        }

        response = self.client.post(url, new_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response_data = response.data
        del(response_data['id'])

        new_user_data = {
            'username':     'new_user_w_name',
            'first_name':   'first',
            'last_name':    'last',
            'expenses':     [],
        }

        self.assertEqual(response_data, new_user_data)

        # test login

        self.login('new_user', 'hunter2')

    def test_user(self):
        url = reverse(self.view_name)

        self.login_user()

        # test required parameters
        new_user = {}
        response = self.client.post(url, new_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        new_user = {'username': 'new_user'}
        response = self.client.post(url, new_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        new_user = {'password': 'hunter2'}
        response = self.client.post(url, new_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # test minimal creation
        new_user = {
            'username':     'new_user',
            'password':     'hunter2',
        }

        response = self.client.post(url, new_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response_data = response.data
        del(response_data['id'])

        new_user_data = {
            'username':     'new_user',
            'first_name':   '',
            'last_name':    '',
            'expenses':     [],
        }

        self.assertEqual(response_data, new_user_data)

        # test duplicate
        new_user = {
            'username':     'new_user',
            'password':     'hunter2',
        }

        response = self.client.post(url, new_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # test full creation
        new_user = {
            'username':     'new_user_w_name',
            'password':     'hunter2',
            'first_name':   'first',
            'last_name':    'last',
        }

        response = self.client.post(url, new_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response_data = response.data
        del(response_data['id'])

        new_user_data = {
            'username':     'new_user_w_name',
            'first_name':   'first',
            'last_name':    'last',
            'expenses':     [],
        }

        self.assertEqual(response_data, new_user_data)

        # test login

        self.login('new_user', 'hunter2')
