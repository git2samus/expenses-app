from django.core.urlresolvers import reverse
from rest_framework import status

from expenses.api.test_modules.utils import FixtureTestCase


class UserListTestCase(FixtureTestCase):
    view_name = 'user-list'

    def test_guest(self):
        url = reverse(self.view_name)

        self.logout()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # POST
        new_user = {
            'username': 'new_user',
            'password': 'hunter2',
        }

        response = self.client.post(url, new_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response_data = response.data
        del(response_data['id'])

        new_user_data = {
            'username':     'new_user',
            'first_name':   '',
            'last_name':    '',
            'expenses':     [],
        }

        self.assertEqual(response_data, new_user_data)

        # PUT
        response = self.client.put(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # PATCH
        response = self.client.patch(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user(self):
        url = reverse(self.view_name)

        self.login_user()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(sorted(response.data), sorted((self.user_data, self.admin_data)))

        # POST
        new_user = {
            'username': 'new_user',
            'password': 'hunter2',
        }

        response = self.client.post(url, new_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response_data = response.data
        del(response_data['id'])

        new_user_data = {
            'username':     'new_user',
            'first_name':   '',
            'last_name':    '',
            'expenses':     [],
        }

        self.assertEqual(response_data, new_user_data)

        # PUT
        response = self.client.put(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # PATCH
        response = self.client.patch(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UserDetailTestCase(FixtureTestCase):
    view_name = 'user-detail'

    def test_guest(self):
        url = reverse(self.view_name, kwargs={'pk': self.admin_data['id']})

        self.logout()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # POST
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # PUT
        updated_user = {
            'username':     'new_user_w_name',
            'password':     'hunter2',
            'first_name':   'first',
            'last_name':    'last',
        }

        response = self.client.put(url, updated_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # PATCH
        response = self.client.patch(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_own_profile(self):
        url = reverse(self.view_name, kwargs={'pk': self.user_data['id']})

        self.login_user()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.user_data)

        # POST
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # PUT
        updated_user = {
            'username':     'new_user_w_name',
            'password':     'hunter2',
            'first_name':   'first',
            'last_name':    'last',
        }

        response = self.client.put(url, updated_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        updated_user['id'] = self.user_data['id']
        updated_user['expenses'] = self.user_data['expenses']
        del(updated_user['password'])

        self.assertEqual(response.data, updated_user)

        # credentials changed, reauthenticate
        self.login(
            username='new_user_w_name',
            password='hunter2',
        )

        # PATCH
        updated_fields = {
            'first_name': 'hunter',
            'last_name':  '2',
        }

        response = self.client.patch(url, updated_fields, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        new_data = updated_user.copy()
        new_data.update(updated_fields)

        self.assertEqual(response.data, new_data)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_other_profile(self):
        url = reverse(self.view_name, kwargs={'pk': self.admin_data['id']})

        self.login_user()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.admin_data)

        # POST
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # PUT
        updated_user = {
            'username':     'new_user_w_name',
            'password':     'hunter2',
            'first_name':   'first',
            'last_name':    'last',
        }

        response = self.client.put(url, updated_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # PATCH
        response = self.client.patch(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_admin_other_profile(self):
        url = reverse(self.view_name, kwargs={'pk': self.user_data['id']})

        self.login_admin()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.user_data)

        # POST
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # PUT
        updated_user = {
            'username':     'new_user_w_name',
            'password':     'hunter2',
            'first_name':   'first',
            'last_name':    'last',
        }

        response = self.client.put(url, updated_user, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        updated_user['id'] = self.user_data['id']
        updated_user['expenses'] = self.user_data['expenses']
        del(updated_user['password'])

        self.assertEqual(response.data, updated_user)

        # PATCH
        updated_fields = {
            'first_name': 'hunter',
            'last_name':  '2',
        }

        response = self.client.patch(url, updated_fields, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        new_data = updated_user.copy()
        new_data.update(updated_fields)

        self.assertEqual(response.data, new_data)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_missing_user(self):
        url = reverse(self.view_name, kwargs={'pk': 99})

        self.login_user()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # POST
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # PUT
        response = self.client.put(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # PATCH
        response = self.client.patch(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
