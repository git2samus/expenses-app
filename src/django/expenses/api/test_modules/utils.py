from rest_framework.test import APITestCase


class FixtureTestCase(APITestCase):
    fixtures = ('test_data.json',)

    admin_data = {'id':1,'username':'admin','first_name':'super','last_name':'user','expenses':['http://testserver/api/v1/users/1/expenses/1']}
    user_data  = {'id':2,'username':'user1','first_name':'user','last_name':'one','expenses':['http://testserver/api/v1/users/2/expenses/2','http://testserver/api/v1/users/2/expenses/3']}

    expense1_data = {'id':1,'datetime':'2015-01-01T00:00:00Z','description':'admin expense','amount':'15.00','comment':'admin expenses'}
    expense2_data = {'id':2,'datetime':'2015-01-10T12:00:00Z','description':'expense1','amount':'12.00','comment':''}
    expense3_data = {'id':3,'datetime':'2015-01-20T06:00:00Z','description':'expense2','amount':'35.50','comment':'some expense'}

    def login_user(self):
        self.login('user1', 'user')

    def login_admin(self):
        self.login('admin', 'admin')

    def login(self, username, password):
        self.assertTrue(
            self.client.login(username=username, password=password)
        )

    def logout(self):
        self.client.logout()
