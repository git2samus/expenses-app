from django.core.urlresolvers import reverse
from rest_framework import status

from expenses.api.test_modules.utils import FixtureTestCase


class ExpenseListTestCase(FixtureTestCase):
    view_name = 'expense-list'

    def test_guest(self):
        url = reverse(self.view_name, kwargs={'user_pk': self.admin_data['id']})

        self.logout()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # POST
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # PUT
        response = self.client.put(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # PATCH
        response = self.client.patch(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_user_own_expenses(self):
        url = reverse(self.view_name, kwargs={'user_pk': self.user_data['id']})

        self.login_user()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(sorted(response.data), sorted((self.expense2_data, self.expense3_data)))

        # POST
        new_data = {
            'description': 'another description',
            'amount': '1500.01',
            'datetime': '2001-02-03T04:05:06Z',
            'comment': 'None',
        }

        response = self.client.post(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response_data = response.data
        del(response_data['id'])

        self.assertEqual(response_data, new_data)

        # PUT
        response = self.client.put(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # PATCH
        updated_field = {'description': 'a new description'}

        response = self.client.patch(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_other_expenses(self):
        url = reverse(self.view_name, kwargs={'user_pk': self.admin_data['id']})

        self.login_user()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # POST
        new_data = {
            'description': 'another description',
            'amount': '1500.01',
            'datetime': '2001-02-03T04:05:06Z',
            'comment': 'None',
        }

        response = self.client.post(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # PUT
        response = self.client.put(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # PATCH
        updated_field = {'description': 'a new description'}

        response = self.client.patch(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_admin_other_expenses(self):
        url = reverse(self.view_name, kwargs={'user_pk': self.user_data['id']})

        self.login_admin()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(sorted(response.data), sorted((self.expense2_data, self.expense3_data)))

        # POST
        new_data = {
            'description': 'another description',
            'amount': '1500.01',
            'datetime': '2001-02-03T04:05:06Z',
            'comment': 'None',
        }

        response = self.client.post(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response_data = response.data
        del(response_data['id'])

        self.assertEqual(response_data, new_data)

        # PUT
        response = self.client.put(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # PATCH
        updated_field = {'description': 'a new description'}

        response = self.client.patch(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_missing_user(self):
        url = reverse(self.view_name, kwargs={'user_pk': 99})

        self.login_user()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # POST
        new_data = {
            'description': 'another description',
            'amount': '1500.01',
            'datetime': '2001-02-03T04:05:06Z',
            'comment': 'None',
        }

        response = self.client.post(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # PUT
        response = self.client.put(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # PATCH
        updated_field = {'description': 'a new description'}

        response = self.client.patch(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        self.login_admin()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # POST
        new_data = {
            'description': 'another description',
            'amount': '1500.01',
            'datetime': '2001-02-03T04:05:06Z',
            'comment': 'None',
        }

        response = self.client.post(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response_data = response.data
        del(response_data['id'])

        self.assertEqual(response_data, new_data)

        # PUT
        response = self.client.put(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # PATCH
        updated_field = {'description': 'a new description'}

        response = self.client.patch(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class ExpenseDetailTestCase(FixtureTestCase):
    view_name = 'expense-detail'

    def test_guest(self):
        url = reverse(self.view_name, kwargs={'user_pk': self.user_data['id'], 'pk': self.expense2_data['id']})

        self.logout()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # POST
        response = self.client.post(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # PUT
        response = self.client.put(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # PATCH
        response = self.client.patch(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_user_own_expense(self):
        url = reverse(self.view_name, kwargs={'user_pk': self.user_data['id'], 'pk': self.expense2_data['id']})

        self.login_user()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.expense2_data)

        # POST
        updated_field = {'description': 'a new description'}

        new_data = self.expense2_data.copy()
        new_data.update(updated_field)

        response = self.client.post(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # PATCH
        response = self.client.patch(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, new_data)

        # PUT
        new_data = {
            'description': 'another description',
            'amount': '1500.01',
            'datetime': '2001-02-03T04:05:06Z',
            'comment': 'None',
        }

        response = self.client.put(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        new_data['id'] = self.expense2_data['id']
        self.assertEqual(response.data, new_data)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_other_expense(self):
        url = reverse(self.view_name, kwargs={'user_pk': self.admin_data['id'], 'pk': self.expense1_data['id']})

        self.login_user()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # POST
        updated_field = {'description': 'a new description'}

        new_data = self.expense2_data.copy()
        new_data.update(updated_field)

        response = self.client.post(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # PATCH
        response = self.client.patch(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # PUT
        new_data = {
            'description': 'another description',
            'amount': '1500.01',
            'datetime': '2001-02-03T04:05:06Z',
            'comment': 'None',
        }

        response = self.client.put(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_admin_other_expense(self):
        url = reverse(self.view_name, kwargs={'user_pk': self.user_data['id'], 'pk': self.expense2_data['id']})

        self.login_admin()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.expense2_data)

        # POST
        updated_field = {'description': 'a new description'}

        new_data = self.expense2_data.copy()
        new_data.update(updated_field)

        response = self.client.post(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # PATCH
        response = self.client.patch(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, new_data)

        # PUT
        new_data = {
            'description': 'another description',
            'amount': '1500.01',
            'datetime': '2001-02-03T04:05:06Z',
            'comment': 'None',
        }

        response = self.client.put(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        new_data['id'] = self.expense2_data['id']
        self.assertEqual(response.data, new_data)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_missing_user(self):
        url = reverse(self.view_name, kwargs={'user_pk': 99, 'pk': self.expense2_data['id']})

        self.login_user()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # POST
        updated_field = {'description': 'a new description'}

        new_data = self.expense2_data.copy()
        new_data.update(updated_field)

        response = self.client.post(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # PATCH
        response = self.client.patch(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # PUT
        new_data = {
            'description': 'another description',
            'amount': '1500.01',
            'datetime': '2001-02-03T04:05:06Z',
            'comment': 'None',
        }

        response = self.client.put(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        self.login_admin()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # POST
        updated_field = {'description': 'a new description'}

        new_data = self.expense2_data.copy()
        new_data.update(updated_field)

        response = self.client.post(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # PATCH
        response = self.client.patch(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # PUT
        new_data = {
            'description': 'another description',
            'amount': '1500.01',
            'datetime': '2001-02-03T04:05:06Z',
            'comment': 'None',
        }

        response = self.client.put(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_missing_expense(self):
        url = reverse(self.view_name, kwargs={'user_pk': self.user_data['id'], 'pk': 99})

        self.login_user()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # POST
        updated_field = {'description': 'a new description'}

        new_data = self.expense2_data.copy()
        new_data.update(updated_field)

        response = self.client.post(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # PATCH
        response = self.client.patch(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # PUT
        new_data = {
            'description': 'another description',
            'amount': '1500.01',
            'datetime': '2001-02-03T04:05:06Z',
            'comment': 'None',
        }

        response = self.client.put(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.login_admin()

        # GET
        response = self.client.get(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # POST
        updated_field = {'description': 'a new description'}

        new_data = self.expense2_data.copy()
        new_data.update(updated_field)

        response = self.client.post(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # PATCH
        response = self.client.patch(url, updated_field, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # PUT
        new_data = {
            'description': 'another description',
            'amount': '1500.01',
            'datetime': '2001-02-03T04:05:06Z',
            'comment': 'None',
        }

        response = self.client.put(url, new_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # DELETE
        response = self.client.delete(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # OPTIONS
        response = self.client.options(url, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
