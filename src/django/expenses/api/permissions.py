from rest_framework.permissions import BasePermission, IsAuthenticated, AllowAny, SAFE_METHODS


class Permission(BasePermission):
    def _is_authenticated(self, request, view, obj=None):
        return request.user.is_authenticated()

    def _is_superuser(self, request, view, obj=None):
        return request.user.is_superuser

    def _is_safe_method(self, request, view, obj=None):
        return request.method in SAFE_METHODS

    def _is_create_action(self, request, view, obj=None):
        return view.action == 'create'

    def _is_update_action(self, request, view, obj=None):
        return any((
            view.action in ('update', 'partial_update'),
            request.method == 'POST' and view.action == None,
        ))

    def _is_own_record(self, request, view, obj=None):
        return request.user == obj

    def _is_own_expense(self, request, view, obj=None):
        return unicode(request.user.pk) == view.kwargs['user_pk']


class UserPerms(Permission):
    def has_permission(self, request, view):
        if request.user:
            return any((
                all((
                    self._is_authenticated(request, view),
                    any((
                        self._is_update_action(request, view),
                        self._is_superuser(request, view),
                    )),
                )),
                self._is_safe_method(request, view),
                self._is_create_action(request, view),
            ))

    def has_object_permission(self, request, view, obj):
        if request.user:
            return any((
                all((
                    self._is_authenticated(request, view, obj),
                    any((
                        all((
                            self._is_own_record(request, view, obj),
                            self._is_update_action(request, view),
                        )),
                        self._is_superuser(request, view, obj),
                    )),
                )),
                self._is_safe_method(request, view, obj),
            ))


class ExpensePerms(Permission):
    def has_permission(self, request, view):
        if request.user:
            return all((
                self._is_authenticated(request, view),
                any((
                    self._is_own_expense(request, view),
                    self._is_superuser(request, view),
                )),
            ))

    def has_object_permission(self, request, view, obj):
        if request.user:
            return all((
                self._is_authenticated(request, view, obj),
                any((
                    self._is_own_expense(request, view, obj),
                    self._is_superuser(request, view, obj),
                )),
            ))
