from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException

from django.contrib.staticfiles.handlers import StaticFilesHandler

from rest_framework.test import APILiveServerTestCase


class SeleniumTestCase(APILiveServerTestCase):
    static_handler = StaticFilesHandler

    fixtures = ('test_data.json',)

    @classmethod
    def setUpClass(cls):
        cls.wd = webdriver.Firefox()
        cls.wd.implicitly_wait(5) # seconds
        super(SeleniumTestCase, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        cls.wd.quit()
        super(SeleniumTestCase, cls).tearDownClass()

    def __getattr__(self, attr):
        # forward missing methods to webdriver
        return getattr(self.wd, attr)

    def find(self, method, selector, scope=None):
        target = self.wd if scope is None else scope
        try:
            return target.find_element(method, selector)
        except NoSuchElementException, e:
            self.fail(msg=e.msg)

    def find_by_id(self, selector, scope=None):
        return self.find(By.ID, selector, scope=scope)

    def find_by_name(self, selector, scope=None):
        return self.find(By.NAME, selector, scope=scope)

    def find_by_xpath(self, selector, scope=None):
        return self.find(By.XPATH, selector, scope=scope)

    def find_by_link_text(self, selector, scope=None):
        return self.find(By.LINK_TEXT, selector, scope=scope)

    def find_by_partial_link_text(self, selector, scope=None):
        return self.find(By.LINK_PARTIAL_TEXT, selector, scope=scope)

    def find_by_tag_name(self, selector, scope=None):
        return self.find(By.TAG_NAME, selector, scope=scope)

    def find_by_class_name(self, selector, scope=None):
        return self.find(By.CLASS_NAME, selector, scope=scope)

    def find_by_css_selector(self, selector, scope=None):
        return self.find(By.CSS_SELECTOR, selector, scope=scope)


class E2ETestCase(SeleniumTestCase):
    def test_layout(self):
        self.get('{host}{path}'.format(host=self.live_server_url, path='/'))

        self.assertTrue('ExpensesApp' in self.title)

        navbar_greeting = self.find_by_id('navbar-greeting')

        login_link = self.find_by_link_text('Login', navbar_greeting)
        register_link = self.find_by_link_text('Register', navbar_greeting)
