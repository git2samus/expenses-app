'use strict';


var loginController = function($scope, $rootScope, $http, $location, apiEndpoints) {
  // form variables
  $scope.credentials = {
    username: '',
    password: ''
  };
  $scope.errors = '';

  // login form submit fn
  $scope.login = function(credentials) {
    $http.post(
      apiEndpoints.login, credentials
    ).success(function(data, status, headers, config) {
      $rootScope.setProfile(data);

      $location.path('/');
    }).error(function(data, status, headers, config) {
      if (status == 403) {
        $scope.errors = 'Invalid credentials';
      } else {
        $location.path('/error');
      };
    });
  };
};


var registerController = function($scope, $resource, apiEndpoints) {
  // form variables
  $scope.credentials = {
    username:         '',
    first_name:       '',
    last_name:        '',
    password:         '',
    password_confirm: ''
  };
  $scope.errors = '';
  $scope.success = false;

  var User = $resource(apiEndpoints.users, {userId: '@id'});

  // form submit fn
  $scope.register = function(credentials) {
    if (credentials.password == credentials.password_confirm) {
      var newUser = new User(credentials);
      newUser.$save(function() {
        $scope.success = true;
      }, function(response) {
        $scope.errors = response.data;
      });
    } else {
      $scope.errors = 'Passwords do not match';
    };
  };
};


var expenseListController = function($scope, $rootScope, apiEndpoints) {
  // filter form variables
  $scope.filters = {
    'from-date':    null,
    'to-date':      null,
    'description':  '',
    'comment':      ''
  };

  // edit/delete buttons
  $scope.createExpense = function(expense) {
    // unset id on DOM for the modal to create new instance
    $('#newExpenseModal').attr('data-edit-id', null);

    // show modal
    $('#newExpenseModal').modal('show');
  };
  $scope.editExpense = function(expense) {
    // set id to edit into DOM property for the modal to pick
    $('#newExpenseModal').attr('data-edit-id', expense.id);

    // show modal
    $('#newExpenseModal').modal('show');
  };
  $scope.deleteExpense = function(expense) {
    if (confirm("Delete Expense " + expense.description + '?')) {
      expense.$delete(function() {
        $scope.applyFilters($scope.filters);
      });
    };
  };


  // filter form submit fn
  $scope.applyFilters = function(filters) {
    // prevent accesing before the profile is loaded
    if ($rootScope.Expense) {
      $scope.expenses = $rootScope.Expense.query(filters);
    };
  };
  $scope.$on('refresh-expenses', function() {
    $scope.applyFilters($scope.filters);
  });

  // display controls
  $scope.setDateRange = function(fromDate, toDate) {
    // set filter range according to datepicker parsing rules

    // .change() triggers Angular model refresh
    $('#from-date').datepicker('setDate', fromDate).change();
    $('#to-date').datepicker('setDate', toDate).change();

    // refresh listing
    $scope.applyFilters($scope.filters);
  };
  $scope.setCurrWeek = function() {
    // current week is from past Sunday to next Saturday
    var dow = (new Date()).getDay();

    // datepicker takes str-numbers as delta days from now
    $scope.setDateRange(String(-dow), String(6-dow));
  };

  $scope.shiftDate = function(factor, calendarId) {
    // shift calendar by factor-weeks
    var dayMs = 24*60*60*1000;

    var $calendar = $(calendarId);
    var newDate = new Date($calendar.datepicker('getDate').getTime() + factor*dayMs);

    // .change() triggers Angular model refresh
    $calendar.datepicker('setDate', newDate).change();

    // refresh listing
    $scope.applyFilters($scope.filters);
  };
  $scope.setPrevWeek = function() {
    // shift current range by 7 days
    $scope.shiftDate(-7, '#from-date');
    $scope.shiftDate(-7, '#to-date');
  };
  $scope.setNextWeek = function() {
    // shift current range by 7 days
    $scope.shiftDate(7, '#from-date');
    $scope.shiftDate(7, '#to-date');
  };

  $scope.printExpenseList = function(filters) {
    // prepare GET request
    var url = apiEndpoints.print.replace(':userId', $rootScope.userProfile.id);

    filters['from-date'] = filters['from-date'].toISOString();
    filters['to-date'] = filters['to-date'].toISOString();
    var params = $.param(filters);

    // popup special view for printing
    window.open(url + '?' + params);
  };
};


var expenseFilterController = function($scope) {
  // initialize calendars
  $('#from-date, #to-date').each(function(index, elem) {
    var $elem = $(elem);

    // setup datepicker
    $elem.datepicker();

    // manually update ng-model
    $elem.change(function(ev) {
      $scope.filters[$elem.attr('id')] = $elem.datepicker('getDate');
    });

    // make calendar icon clickable
    $elem.next().click(function(ev) {
      $elem.datepicker('show');
    });
  });

  // callback from login
  $scope.$on('setProfile', function(ev) {
    $scope.applyFilters($scope.filters);
  });


  // set initial values
  $scope.setCurrWeek();
  // request initial listing
  $scope.applyFilters($scope.filters);
};


var newExpenseController = function($scope, $rootScope, apiEndpoints) {
  // form variables
  var today = new Date();
  $scope.expenseData = {
    year:        today.getFullYear(),
    month:       today.getMonth(),
    day:         today.getDate(),
    hour:        today.getHours(),
    minute:      today.getMinutes(),
    description: '',
    amount:      0,
    comment:     ''
  };
  $scope.errors = {};

  // form submit fn
  $scope.createExpense = function(expenseData) {
    var newExpense = new $rootScope.Expense({
      datetime:    new Date(expenseData.year, expenseData.month, expenseData.day, expenseData.hour, expenseData.minute),
      description: expenseData.description,
      amount:      expenseData.amount,
      comment:     expenseData.comment
    });
    newExpense.$save(function() {
      $scope.errors = {};
      $('#newExpenseModal').modal('hide');

      $rootScope.$broadcast('refresh-expenses');
    }, function(response) {
      $scope.errors = response.data;
    });
  };
  $scope.updateExpense = function(expenseData) {
    $scope.editExpense.datetime    = new Date(expenseData.year, expenseData.month, expenseData.day, expenseData.hour, expenseData.minute);
    $scope.editExpense.description = expenseData.description;
    $scope.editExpense.amount      = expenseData.amount;
    $scope.editExpense.comment     = expenseData.comment;

    $rootScope.Expense.update(
      {expenseId: $scope.editExpense.id},
      $scope.editExpense,
      function() {
        $scope.errors = {};
        $('#newExpenseModal').modal('hide');

        $rootScope.$broadcast('refresh-expenses');
      }, function(response) {
        $scope.errors = response.data;
      }
    );
  };

  // callback on show event to detect if we're creating or editing an instance
  $('#newExpenseModal').on('show.bs.modal', function(ev) {
    var editId = $(this).attr('data-edit-id');

    if (editId) {
      // save instance to be updated
      $scope.editExpense = $rootScope.Expense.get({expenseId: editId}, function(editExpense) {
        // prefill forms with current data
        var editExpenseDate = new Date(editExpense.datetime);
        $scope.expenseData = {
          year:        editExpenseDate.getFullYear(),
          month:       editExpenseDate.getMonth(),
          day:         editExpenseDate.getDate(),
          hour:        editExpenseDate.getHours(),
          minute:      editExpenseDate.getMinutes(),
          description: editExpense.description,
          amount:      parseFloat(editExpense.amount),
          comment:     editExpense.comment
        };
      });
    } else {
      // set defaults for new instance
      var today = new Date();
      $scope.expenseData = {
        year:        today.getFullYear(),
        month:       today.getMonth(),
        day:         today.getDate(),
        hour:        today.getHours(),
        minute:      today.getMinutes(),
        description: '',
        amount:      0,
        comment:     ''
      };
      $scope.editExpense = null;
    };
  });

  // manualy setup save event because Angular conflicts
  $('#save-new-expense').click(function(ev) {
    if ($scope.editExpense) {
      $scope.updateExpense($scope.expenseData);
    } else {
      $scope.createExpense($scope.expenseData);
    };
  });
};


// module config

var expensesControllers = angular.module('expensesControllers', [
  'ngResource'
]);

expensesControllers.controller('loginController', [
  '$scope',
  '$rootScope',
  '$http',
  '$location',
  'apiEndpoints',
  loginController
]);

expensesControllers.controller('registerController', [
  '$scope',
  '$resource',
  'apiEndpoints',
  registerController
]);

expensesControllers.controller('expenseListController', [
  '$scope',
  '$rootScope',
  'apiEndpoints',
  expenseListController
]);

expensesControllers.controller('expenseFilterController', [
  '$scope',
  expenseFilterController
]);

expensesControllers.controller('newExpenseController', [
  '$scope',
  '$rootScope',
  'apiEndpoints',
  newExpenseController
]);
