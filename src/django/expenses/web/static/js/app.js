'use strict';


var routeProviderCfg = function($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: 'static/partials/expense-list.html',
    controller: 'expenseListController'
  });

  $routeProvider.when('/login', {
    templateUrl: 'static/partials/login.html',
    controller: 'loginController'
  });

  $routeProvider.when('/register', {
    templateUrl: 'static/partials/register.html',
    controller: 'registerController'
  });

  $routeProvider.when('/error', {
    templateUrl: 'static/partials/error.html'
  });

  $routeProvider.otherwise({
    redirectTo: '/'
  });
};


var httpProviderCfg = function($httpProvider) {
  // have Angular use Django naming conventions
  $httpProvider.defaults.xsrfCookieName = 'csrftoken';
  $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
};


var initFn = function($rootScope, $http, $resource, $location, apiEndpoints) {
  $rootScope.setProfile = function(userProfile) {
    $rootScope.userProfile = userProfile;
    $rootScope.Expense = $resource(
      apiEndpoints.expenses,
      {userId: userProfile.id, expenseId: '@id'},
      {'update': {method: 'PUT'}}
    );

    $rootScope.$broadcast('setProfile');
  };

  $rootScope.unsetProfile = function() {
    $rootScope.userProfile = null;
    $rootScope.Expense = null;

    $rootScope.$broadcast('unsetProfile');
  };

  $rootScope.logout = function() {
    $http.get(
      apiEndpoints.logout
    ).success(function(data, status, headers, config) {
      $rootScope.unsetProfile();

      $location.path('/login');
    }).error(function(data, status, headers, config) {
      $location.path('/error');
    });
  };

  // load profile
  $http.get(
    apiEndpoints.me
  ).success(function(data, status, headers, config) {
    $rootScope.setProfile(data);

    if ($location.path() == '/login') {
      $location.path('/');
    };
  }).error(function(data, status, headers, config) {
    if (status == 403) {
      $rootScope.userProfile = null;

      $location.path('/login');
    } else {
      $location.path('/error');
    };
  });

  $rootScope.isReady = true;
};


// module config

var expensesApp = angular.module('expensesApp', [
  'expensesControllers',
  'ngRoute'
]);

var apiRoot = '/api/v1';
expensesApp.constant('apiEndpoints', {
  me:       apiRoot + '/me.json',
  login:    apiRoot + '/api-auth/login.json',
  logout:   apiRoot + '/api-auth/logout',
  users:    apiRoot + '/users/:userId.json',
  expenses: apiRoot + '/users/:userId/expenses/:expenseId.json',
  print:    apiRoot + '/users/:userId/expenses/print_report'
});

expensesApp.config(['$httpProvider', httpProviderCfg]);
expensesApp.config(['$routeProvider', routeProviderCfg]);

expensesApp.run([
  '$rootScope',
  '$http',
  '$resource',
  '$location',
  'apiEndpoints',
  initFn
]);
