from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = patterns('',
    url(r'^admin/',     include(admin.site.urls)),
    url(r'^api/v1/',    include('expenses.api.urls')),
    url(r'^',           include('expenses.web.urls')),
) + staticfiles_urlpatterns()
