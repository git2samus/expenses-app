Django==1.7.4
argparse==1.2.1
django-template-utils==0.1.1
djangorestframework==3.0.4
selenium==2.44.0
wsgiref==0.1.2
